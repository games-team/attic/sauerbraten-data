#!/bin/sh

RELEASEDATE=20130203
SVNREV=4711

svn co -r ${SVNREV} \
	https://sauerbraten.svn.sourceforge.net/svnroot/sauerbraten \
	sauerbraten-data-0.0.${RELEASEDATE}

find sauerbraten-data-0.0.${RELEASEDATE} -type d -name '.svn' \
	-exec rm -rf "{}" \;
rm -rf sauerbraten-data-0.0.${RELEASEDATE}/vcpp
rm -rf sauerbraten-data-0.0.${RELEASEDATE}/xcode
rm -rf sauerbraten-data-0.0.${RELEASEDATE}/lib
rm -rf sauerbraten-data-0.0.${RELEASEDATE}/bin
rm -rf sauerbraten-data-0.0.${RELEASEDATE}/bin_unix
rm -f sauerbraten-data-0.0.${RELEASEDATE}/sauerbraten.bat
rm -f sauerbraten-data-0.0.${RELEASEDATE}/sauerbraten.pdb
rm -f sauerbraten-data-0.0.${RELEASEDATE}/sauerbraten_unix
rm -f sauerbraten-data-0.0.${RELEASEDATE}/server.bat
rm -f sauerbraten-data-0.0.${RELEASEDATE}/server-init.cfg
rm -rf sauerbraten-data-0.0.${RELEASEDATE}/src

tar -czf sauerbraten-data_0.0.${RELEASEDATE}.orig.tar.gz \
	sauerbraten-data-0.0.${RELEASEDATE}

rm -rf sauerbraten-data-0.0.${RELEASEDATE}
